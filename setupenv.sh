if [ "$#" -ge 1 ];
then
	EnvironmentName=$1
else
	EnvironmentName="default"
fi

# set up environmental paths
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done


DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

export HPOGRID_BASE_PATH=${DIR}


export PATH=/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/envs/ml-base/bin:$PATH

if [ "$EnvironmentName" = "dev" ];
then
 export PATH=${DIR}/bin:${PATH}
 export PYTHONPATH=${DIR}:${PYTHONPATH}
fi

if [ "$EnvironmentName" != "test" ];
then
setupATLAS
lsetup panda
lsetup rucio
fi

#! /usr/bin/env python
import sys
import argparse

#from hpogrid import __version__

__version__ = '0.0.1'

class HPOGridParser(argparse.ArgumentParser):
    def error(self, message):
        '''
            Override default behavior on error to print help message instead
        '''
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)
        
class CustomHelpFormatter(argparse.HelpFormatter):
    def _format_action(self, action):
        if type(action) == argparse._SubParsersAction:
            # inject new class variable for subcommand formatting
            subactions = action._get_subactions()
            invocations = [self._format_action_invocation(a) for a in subactions]
            self._subcommand_max_length = max(len(i) for i in invocations)

        if type(action) == argparse._SubParsersAction._ChoicesPseudoAction:
            # format subcommand help line
            subcommand = self._format_action_invocation(action) # type: str
            width = self._subcommand_max_length
            help_text = ""
            if action.help:
                help_text = self._expand_help(action)
            return "  {:{width}}     {}\n".format(subcommand, help_text, width=width)

        elif type(action) == argparse._SubParsersAction:
            # process subcommand help section
            msg = '\n'
            for subaction in action._get_subactions():
                msg += self._format_action(subaction)
            return msg
        else:
            return super(CustomHelpFormatter, self)._format_action(action)

class HPOGridCLI_test():
    def __init__(self):
        parser = HPOGridParser(description='Tool for Hyperparameter Optimization on the Grid',
                               formatter_class=CustomHelpFormatter,
                             usage='''hpogrid [-v|--version] [-h|--help] <command> [<args>]''',
                             add_help=False)
        parser.add_argument('-v', '--version', action='version',
                            version='%(prog)s version {}'.format(__version__), help="show program's version number")
        parser.add_argument('-h', '--help', action="store_true", help='show help')
        
        parser.add_argument('command', help='Subcommand to run', choices=kCommandList)
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail        
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        getattr(self, args.command)()
        
        
def HPOGridCLI():
    parser = HPOGridParser(description='Tool for Hyperparameter Optimization on the Grid',
                           formatter_class=CustomHelpFormatter,
                         usage='''hpogrid [-v|--version] [-h|--help] <command> [<args>]''',
                         add_help=False)
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s version {}'.format(__version__), help="show program's version number")
    parser.add_argument('-h', '--help', action="store_true", help='show help')
    parser._positionals.title = "The most commonly used hpogrid commands are"

    command_parser = parser.add_subparsers(dest="command")
    parser_hpo_config = command_parser.add_parser('hpo_config', help='show status')
    command_parser.add_parser('grid_config', help='print list')
    command_parser.add_parser('model_config', help='show status')
    command_parser.add_parser('search_space', help='print list')
    command_parser.add_parser('project', help='show status')
    command_parser.add_parser('tasks', help='print list')
    command_parser.add_parser('run', help='show status')
    command_parser.add_parser('submit', help='print list')
    command_parser.add_parser('sites', help='show status')
    command_parser.add_parser('report', help='print list')
    command_parser.add_parser('generate', help='print list')
    command_parser.add_parser('idds_log', help='print list')
    action_parser = parser_hpo_config.add_subparsers(dest="action")
    create_parser = action_parser.add_parser('create',help='create hpo config')
    create_parser.add_argument('test', help='final test')
    # hack to show help when no arguments supplied
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    
    args = parser.parse_args()
    print(args.command)
    if args.help:
        parser.print_help()
        sys.exit(0)

        
class CLI(object):
    _COMMAND_INFO_ = {
        'hpo_config': 'Manage HPO configuration',
        'grid_config': 'Manage grid configuration',
        'model_config': 'Manage model configuration',
        'search_space': 'Manage search space',
        'project': 'Manage a project',
        'tasks': 'Monitor grid jobs',
        'run': 'Run an HPO task',
        'submit': 'Submit an HPO task to the grid/idds',
        'sites': 'Show available grid sites',
        'report': 'Display a report of an HPO task',
        'generate' :'Generate hyperparameter search points',
        'idds_log' : 'Download idds log'
    }
    @staticmethod
    def run_parser():
        parser = HPOGridParser(description='Tool for Hyperparameter Optimization on the Grid',
                               formatter_class=CustomHelpFormatter,
                             usage='''hpogrid [-v|--version] [-h|--help] <command> [<args>]''',
                             add_help=False)
        parser.add_argument('-v', '--version', action='version',
                            version='%(prog)s version {}'.format(__version__), help="show program's version number")
        parser.add_argument('-h', '--help', action="store_true", help='show help')
        parser._positionals.title = "The most commonly used hpogrid commands are"
        
        command_parser = parser.add_subparsers(dest="command")
        for command in CLI._COMMAND_INFO_:
            command_parser.add_parser(command, help=CLI._COMMAND_INFO_[command])
        
        # print help if no commands or arguments are given
        if len(sys.argv) < 2:
            parser.print_help()
            sys.exit(0)
        args = parser.parse_args(sys.argv[1:2])
        
        # print help if -h or --help are given
        if args.help:
            parser.print_help()
            sys.exit(0)

        if not hasattr(CLI, args.command):
            print('Unrecognized command')
            parser.print_help()
            sys.exit(0)
        getattr(CLI, args.command)(args=sys.argv[2:])
        
    @staticmethod    
    def hpo_config(args=None):
        print('SUCCESS')
        #from hpogrid import HPOConfiguration
        #HPOConfiguration.run_parser(args)
        
    @staticmethod
    def grid_config(args=None):
        from hpogrid import GridConfiguration
        GridConfiguration.run_parser(args)
        
    @staticmethod
    def search_space(args=None):
        from hpogrid import SearchSpaceConfiguration
        SearchSpaceConfiguration.run_parser(args)

    @staticmethod
    def model_config(args=None):
        from hpogrid import ModelConfiguration
        ModelConfiguration.run_parser(args)

    @staticmethod
    def project(args=None):
        from hpogrid import ProjectConfiguration
        ProjectConfiguration.run_parser(args)
    
    @staticmethod
    def tasks(args=None):
        from hpogrid.components.panda_task_manager import PandaTaskManager
        PandaTaskManager.run_parser(args)
        
    @staticmethod
    def submit(args=None):
        from hpogrid import GridHandler
        GridHandler.run_parser(args)
    
    @staticmethod
    def run(args=None):
        from hpogrid import JobBuilder
        parser = JobBuilder.get_parser()
        args = vars(parser.parse_args(sys.argv[2:]))
        hpogrid.run(**args)

    @staticmethod
    def sites(args=None):
        from hpogrid import GridSiteInfo
        GridSiteInfo.run_parser(args)

    @staticmethod
    def report(args=None):
        from hpogrid.components.reporter import HPOTaskHandle
        HPOTaskHandle.run_parser(args)
        
    @staticmethod
    def generate(args=None):
        from hpogrid import SteeringIDDS
        SteeringIDDS.run_parser(args)
    
    @staticmethod
    def idds_log(args=None):
        parser = argparse.ArgumentParser(
            description='Tool for fetching iDDS log files',
            usage='''hpogrid idds_log <task_id>''')
        parser.add_argument('task_id', help='Task ID for iDDS job', type=int)      
        args = parser.parse_args(sys.argv[2:])
        from hpogrid.idds_interface.idds_utils import check_log
        check_log(args.task_id)        
    
if __name__ == '__main__':
    CLI.run_parser()